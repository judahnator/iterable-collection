Changelog
=========

v1.4.0
------

Resolving deprecation notice in newer versions of PHP.
Minor version bump due to signature change, though minor.

v1.3.1
------

Fixing a few issues where the `chunk` method would not properly chunk.

v1.3.0
------

Adding a `any` and `all` methods.
```php
$o = IterableCollection::wrap([true, false, true]);
$o->all(); // false
$o->any(); // true

IterableCollection::wrap([1, 2, 3])
    ->all('is_numeric');
# true

IterableCollection::wrap(['a', 'b', 'c'])
    ->any(fn ($letter) => $letter === 'd');
# false
```

v1.2.0
------

Implementing a new `IterableCollection::reduce` method.
```php
$o = IterableCollection::wrap([1, 2, 3]);
$o->reduce(static fn (int $carry, int $item): int => $carry + $item, 0);
# 6
```


v1.1.0
------

Implementing a new `IterableCollection::toArray` method.

```php
$iterator = new ArrayIterator(['foo' => 'bar']);
print_r(IterableCollection::wrap($iterator)->toArray());
# Array
# (
#     [foo] => bar
# )
```

v1.0.0
------

Initial public release.