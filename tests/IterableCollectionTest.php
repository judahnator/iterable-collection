<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

/**
 * Class IterableCollectionTest
 * @uses IterableCollection
 * @covers IterableCollection::__construct
 */
final class IterableCollectionTest extends TestCase
{
    /**
     * @covers IterableCollection::all
     */
    public function testAllFunction(): void
    {
        // no callback
        $this->assertTrue(IterableCollection::wrap([true, true, true])->all());
        $this->assertFalse(IterableCollection::wrap([true, false, true])->all());

        // with callback
        $callback = static fn (string $i): bool => in_array($i, ['a', 'b', 'c']);
        $this->assertTrue(IterableCollection::wrap(['a', 'b', 'c'])->all($callback));
        $this->assertFalse(IterableCollection::wrap(['b', 'c', 'd'])->all($callback));
    }

    /**
     * @covers IterableCollection::any
     */
    public function testAnyFunction(): void
    {
        // no callback
        $this->assertTrue(IterableCollection::wrap([false, true, false])->any());
        $this->assertFalse(IterableCollection::wrap([false, false, false])->any());

        // with callback
        $callback = static fn (int $i): bool => $i <= 10;
        $this->assertTrue(IterableCollection::wrap([10, 20, 30])->any($callback));
        $this->assertFalse(IterableCollection::wrap([20, 30, 40])->any($callback));
    }

    /**
     * @covers IterableCollection::changeKeyCase
     * @covers IterableCollection::walk
     */
    public function testChangeKeyCaseFunction(): void
    {
        $input = ["FirSt" => 1, "SecOnd" => 4];

        $this->assertEquals(
            array_change_key_case($input, CASE_LOWER),
            iterator_to_array(IterableCollection::wrap($input)->changeKeyCase(CASE_LOWER))
        );
        $this->assertEquals(
            array_change_key_case($input, CASE_UPPER),
            iterator_to_array(IterableCollection::wrap($input)->changeKeyCase(CASE_UPPER))
        );
    }

    /**
     * @covers IterableCollection::chunk
     */
    public function testChunkFunction(): void
    {
        // No preserve keys
        $this->assertEquals(
            array_chunk([1, 2, 3, 4], 2),
            iterator_to_array(IterableCollection::wrap([1, 2, 3, 4])->chunk(2))
        );

        // Preserve keys
        $this->assertEquals(
            array_chunk(['a', 'b', 'c', 'd', 'e', 'f'], 3, true),
            iterator_to_array(
                IterableCollection::wrap(['a', 'b', 'c', 'd', 'e', 'f'])->chunk(3, true)
            )
        );

        // More than two chunks
        $this->assertEquals(
            array_chunk(range(1, 10), 2),
            iterator_to_array(
                IterableCollection::wrap(range(1, 10))->chunk(2)
            )
        );

        // With chunk leftovers
        $this->assertEquals(
            array_chunk(range(1, 5), 2),
            iterator_to_array(
                IterableCollection::wrap(range(1, 5))->chunk(2)
            )
        );
    }

    /**
     * @covers IterableCollection::column
     */
    public function testColumnFunction(): void
    {
        $input = [
            [
                'id' => 2135,
                'first_name' => 'John',
                'last_name' => 'Doe',
            ],
            [
                'id' => 3245,
                'first_name' => 'Sally',
                'last_name' => 'Smith',
            ],
            [
                'id' => 5342,
                'first_name' => 'Jane',
                'last_name' => 'Jones',
            ],
            [
                'id' => 5623,
                'first_name' => 'Peter',
                'last_name' => 'Doe',
            ]
        ];

        $this->assertEquals(
            array_column($input, 'first_name'),
            iterator_to_array(IterableCollection::wrap($input)->column('first_name'))
        );

        $this->assertEquals(
            array_column($input, 'last_name', 'id'),
            iterator_to_array(IterableCollection::wrap($input)->column('last_name', 'id'))
        );

        // Test with objects
        $input = [
            (object)[
                'foo' => 'bar',
                'bing' => 'baz'
            ],
            (object)[
                'foo' => 'whatever',
                'bing' => 'whatever2'
            ]
        ];

        $this->assertEquals(
            array_column($input, 'foo'),
            iterator_to_array(IterableCollection::wrap($input)->column('foo'))
        );

        $this->assertEquals(
            array_column($input, 'foo', 'bing'),
            iterator_to_array(IterableCollection::wrap($input)->column('foo', 'bing'))
        );
    }

    /**
     * @covers IterableCollection::combine
     */
    public function testCombineFunction(): void
    {
        $this->assertEquals(
            array_combine(['a', 'b', 'c'], ['A', 'B', 'C']),
            iterator_to_array(IterableCollection::wrap(['a', 'b', 'c'])->combine(['A', 'B', 'C']))
        );
    }

    /**
     * @covers IterableCollection::count
     */
    public function testCountFunction(): void
    {
        $this->assertEquals(
            count([]),
            IterableCollection::wrap([])->count()
        );

        $this->assertEquals(
            count(range(1, 5)),
            IterableCollection::wrap(range(1, 5))->count()
        );
    }

    /**
     * @covers IterableCollection::countValues
     */
    public function testCountValuesFunction(): void
    {
        $input = [1, "hello", 1, "world", "hello"];
        $this->assertEquals(
            array_count_values($input),
            iterator_to_array(IterableCollection::wrap($input)->countValues())
        );
    }

    /**
     * @covers IterableCollection::__debugInfo
     */
    public function testDebugInfoFunction(): void
    {
        $this->assertEquals(
            <<<TEXT
            IterableCollection Object
            (
                [foo] => bar
                [bing] => baz
            )
            
            TEXT,
            print_r(IterableCollection::wrap(['foo' => 'bar', 'bing' => 'baz']), true),
        );
    }

    /**
     * @covers IterableCollection::fillKeys
     */
    public function testFillKeysFunction(): void
    {
        $this->assertEquals(
            array_fill_keys(['foo', 'bar'], 'baz'),
            iterator_to_array(IterableCollection::wrap(['foo', 'bar'])->fillKeys('baz'))
        );
    }

    /**
     * @covers IterableCollection::filter
     */
    public function testFilterFunction(): void
    {
        // First with no callback
        $input = [0, 1, 2, true, false, 'asdf'];
        $this->assertEquals(
            array_filter($input),
            iterator_to_array(IterableCollection::wrap($input)->filter())
        );

        // Next with callback
        $input = range(0, 10);
        $callback = function ($input): bool {
            return $input % 2 === 0;
        };
        $this->assertEquals(
            array_filter($input, $callback),
            iterator_to_array(IterableCollection::wrap($input)->filter($callback))
        );
    }

    /**
     * @covers IterableCollection::flip
     */
    public function testFlipFunction(): void
    {
        $input = [
            'k1' => 'v1',
            'k2' => 'v2',
            'k3' => 'v3'
        ];
        $this->assertEquals(
            array_flip($input),
            iterator_to_array(IterableCollection::wrap($input)->flip())
        );
    }

    /**
     * @covers IterableCollection::in
     */
    public function testInFunction(): void
    {
        $input = ['foo', 'bar', 'bing'];
        $this->assertEquals(
            in_array('bar', $input),
            IterableCollection::wrap($input)->in('bar')
        );

        $input = range(1, 5);
        $this->assertEquals(
            in_array(6, $input),
            IterableCollection::wrap($input)->in(6)
        );

        $input = [
            [1, 2, 3],
            ['a', 'b', 'c'],
        ];
        $this->assertEquals(
            in_array(['a', 'b', 'c'], $input),
            IterableCollection::wrap($input)->in(['a', 'b', 'c'])
        );
    }

    /**
     * @covers IterableCollection::getIterator
     */
    public function testIteration(): void
    {
        $input = ['foo' => 'bar', 'bing' => 'baz'];
        foreach (IterableCollection::wrap($input) as $k => $v) {
            $this->assertArrayHasKey($k, $input);
            $this->assertEquals($input[$k], $v);
        }
    }

    /**
     * @covers IterableCollection::keyExists
     * @covers IterableCollection::keys
     */
    public function testKeyExistsFunction(): void
    {
        // Should be true
        $this->assertTrue(
            IterableCollection::wrap(['exists' => 'asdf', 'bar' => 'baz'])->keyExists('exists')
        );

        // Should be false
        $this->assertFalse(
            IterableCollection::wrap(['foo' => 'bar'])->keyExists('doesNotExist')
        );
    }

    /**
     * @covers IterableCollection::keys
     */
    public function testKeysFunction(): void
    {
        $input = [
            'k1' => 'v1',
            'k2' => 'v2',
            'k3' => 'v3'
        ];
        $this->assertEquals(
            array_keys($input),
            iterator_to_array(IterableCollection::wrap($input)->keys())
        );
    }

    /**
     * @covers IterableCollection::map
     */
    public function testMapFunction(): void
    {
        $squareFunction = function (int $value): int {
            return $value**2;
        };
        $input = [1, 2, 4, 8];
        $this->assertEquals(
            array_map(
                $squareFunction,
                $input
            ),
            iterator_to_array(IterableCollection::wrap($input)->map($squareFunction))
        );
    }

    /**
     * @covers IterableCollection::reduce
     */
    public function testReduceFunction(): void
    {
        // Ensures the $initial works
        $this->assertNull(
            IterableCollection::wrap([])->reduce(static fn () => null),
        );
        // basic reduction
        $this->assertEquals(
            6,
            IterableCollection::wrap([1, 2, 3])->reduce(static fn (int $carry, int $item): int => $carry + $item, 0),
        );
    }

    /**
     * @covers IterableCollection::toArray
     */
    public function testToArrayFunction(): void
    {
        $iterator = new ArrayIterator(['foo' => 'bar', 'bing' => 'baz']);
        $this->assertEquals(
            iterator_to_array($iterator),
            IterableCollection::wrap($iterator)->toArray(),
        );
        $this->assertEquals(
            iterator_to_array($iterator, false),
            IterableCollection::wrap($iterator)->toArray(false),
        );
    }

    /**
     * @covers IterableCollection::values
     */
    public function testValuesFunction(): void
    {
        $input = [
            'k1' => 'v1',
            'k2' => 'v2',
            'k3' => 'v3'
        ];
        $this->assertEquals(
            array_values($input),
            iterator_to_array(IterableCollection::wrap($input)->values())
        );
    }

    /**
     * @covers IterableCollection::walkRecursive
     */
    public function testWalkRecursiveFunction(): void
    {
        $fruits = ['sweet' => ['a' => 'apple', 'b' => 'banana'], 'sour' => 'lemon'];
        $expected = [];
        array_walk_recursive(
            $fruits,
            function ($item, $key, $prefix) use (&$expected): void {
                $expected[] = "{$prefix} {$key}: {$item}";
            },
            'prefix'
        );
        $callback = function (string $item, string $key, string $prefix) {
            return "{$prefix} {$key}: {$item}";
        };
        $this->assertEquals(
            $expected,
            iterator_to_array(IterableCollection::wrap($fruits)->walkRecursive($callback, 'prefix'))
        );
    }

    /**
     * @covers IterableCollection::walk
     */
    public function testWalkFunction(): void
    {
        $fruits = ["d" => "lemon", "a" => "orange", "b" => "banana", "c" => "apple"];
        $e1 = $fruits;

        $callback = function (&$item, $key, $prefix): void {
            $item = "{$prefix}: [{$key}] {$item}";
        };

        array_walk($e1, $callback, 'prefix');
        $e2 = iterator_to_array(IterableCollection::wrap($fruits)->walk($callback, 'prefix'));

        $this->assertEquals($e1, $e2);
    }

    /**
     * @covers IterableCollection::__construct
     * @covers IterableCollection::wrap
     */
    public function testWrapFunction(): void
    {
        $wrapped = IterableCollection::wrap(['foo' => 'bar']);
        $this->assertInstanceOf(IterableCollection::class, $wrapped);
        $this->assertEquals(
            ['foo' => 'bar'],
            iterator_to_array($wrapped),
        );
    }
}
