<?php

declare(strict_types=1);

/**
 * Class IterableCollection
 * @template K
 * @template V
 * @implements IteratorAggregate<K, V>
 */
final class IterableCollection implements IteratorAggregate
{
    /**
     * @var iterable<K, V>
     */
    private iterable $iterator;

    /**
     * IterableCollection constructor.
     * @param iterable<K, V> $iterator
     */
    public function __construct(iterable $iterator)
    {
        $this->iterator = $iterator;
    }

    /**
     * @return array<K, V>
     */
    public function __debugInfo(): array
    {
        return $this->toArray();
    }

    /**
     * @return Traversable<K, V>
     */
    public function getIterator(): Traversable
    {
        yield from $this->iterator;
    }

    /**
     * Returns true if all elements are true or satisfies a given condition.
     *
     * @param null|callable(V,K):bool $satisfies
     * @return bool
     */
    public function all(callable $satisfies = null): bool
    {
        foreach ($this->map($satisfies ?? static fn ($in): bool => (bool)$in) as $value) {
            if (!$value) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns true if any element is true or satisfies a given condition.
     *
     * @param null|callable(V,K):bool $satisfies
     * @return bool
     */
    public function any(callable $satisfies = null): bool
    {
        foreach ($this->map($satisfies ?? static fn ($in): bool => (bool)$in) as $value) {
            if ($value) {
                return true;
            }
        }
        return false;
    }

    /**
     * Changes the case of all elements in the list.
     *
     * @see https://www.php.net/manual/en/function.array-change-key-case.php
     *
     * @param int $case
     * @return IterableCollection<string, V>
     */
    public function changeKeyCase(int $case = CASE_LOWER): self
    {
        return $this->walk(static function ($_, &$key) use ($case): void {
            $key = $case === CASE_LOWER ? strtolower($key) : strtoupper($key);
        });
    }

    /**
     * Split the iterator into chunks
     *
     * @see https://www.php.net/manual/en/function.array-chunk.php
     *
     * @param int $size
     * @param bool $preserve_keys
     * @return IterableCollection<int|K, array<V>>
     */
    public function chunk(int $size, bool $preserve_keys = false): self
    {
        return new self((function () use ($size, $preserve_keys): Generator {
            [$index, $chunk] = [0, []];
            foreach ($this->iterator as $key => $item) {
                $preserve_keys ? $chunk[$key] = $item : $chunk[] = $item;
                if (++$index === $size) {
                    yield $chunk;
                    [$index, $chunk] = [0, []];
                }
            }
            if (!empty($chunk)) {
                yield $chunk;
            }
        })());
    }

    /**
     * Return the values from a single column in the input.
     *
     * @see https://www.php.net/manual/en/function.array-column.php
     *
     * @param mixed $column
     * @param mixed|null $index_key
     * @return IterableCollection<int|string, mixed|V>
     */
    public function column($column, $index_key = null): self
    {
        return new self((function () use ($column, $index_key): Generator {
            if ($index_key) {
                foreach ($this->iterator as $item) {
                    if (is_array($item) || $item instanceof ArrayAccess) {
                        yield $item[$index_key] => $item[$column];
                    } else {
                        yield $item->{$index_key} => $item->{$column};
                    }
                }
            } else {
                foreach ($this->iterator as $item) {
                    if (is_array($item) || $item instanceof ArrayAccess) {
                        yield $item[$column];
                    } else {
                        yield $item->{$column};
                    }
                }
            }
        })());
    }

    /**
     * Counts the number of values in the iterable.
     *
     * @return int
     */
    public function count(): int
    {
        return iterator_count($this);
    }

    /**
     * Creates a collection by using the current for keys and another for its values.
     *
     * @template T
     * @param iterable<T> $values
     * @return IterableCollection<V, T>
     */
    public function combine(iterable $values): self
    {
        return new self((function () use ($values): Generator {
            $keyGenerator = (fn (): Generator => yield from $this->iterator)();
            $valueGenerator = (static fn (): Generator => yield from $values)();
            while ($keyGenerator->valid() && $valueGenerator->valid()) {
                yield $keyGenerator->current() => $valueGenerator->current();
                $keyGenerator->next();
                $valueGenerator->next();
            }
        })());
    }

    /**
     * Counts all the values of the collection.
     *
     * @see https://www.php.net/manual/en/function.array-count-values.php
     *
     * @return IterableCollection<V, int>
     */
    public function countValues(): self
    {
        $result = [];
        foreach ($this->iterator as $item) {
            if (!array_key_exists($item, $result)) {
                $result[$item] = 0;
            }
            $result[$item]++;
        }
        return new self($result);
    }

    /**
     * Fill a collection, using the current as keys and the input as values.
     *
     * @see https://www.php.net/manual/en/function.array-fill-keys.php
     *
     * @template J
     * @param J $value
     * @return IterableCollection<V, J>
     */
    public function fillKeys($value): self
    {
        return new self((function () use ($value): Generator {
            foreach ($this->iterator as $newKey) {
                yield $newKey => $value;
            }
        })());
    }

    /**
     * Filters the contents of the current collection using a callback.
     *
     * @see https://www.php.net/manual/en/function.array-filter.php
     *
     * @param (callable(V, K): bool)|null $filterCallback
     * @return IterableCollection<K, V>
     */
    public function filter(callable $filterCallback = null): self
    {
        $filterCallback ??= static fn ($value, $_): bool => (bool)$value;
        return new self((function () use ($filterCallback): Generator {
            foreach ($this->iterator as $key => $item) {
                if ((bool)$filterCallback($item, $key)) {
                    yield $key => $item;
                }
            }
        })());
    }

    /**
     * Exchanges all current keys and values.
     *
     * @see https://www.php.net/manual/en/function.array-flip.php
     *
     * @return IterableCollection<V, K>
     */
    public function flip(): self
    {
        return new self((function (): Generator {
            foreach ($this->iterator as $key => $value) {
                yield $value => $key;
            }
        })());
    }

    /**
     * Checks if a value exists in the collection.
     *
     * @see https://www.php.net/manual/en/function.in-array.php
     *
     * @param V $needle
     * @return bool
     */
    public function in($needle): bool
    {
        return $this->any(static fn ($item, $_): bool => $item === $needle);
    }

    /**
     * Checks if a given key exists in the collection.
     *
     * @see https://www.php.net/manual/en/function.array-key-exists.php
     *
     * @param K $key
     * @return bool
     */
    public function keyExists($key): bool
    {
        return $this->keys()->in($key);
    }

    /**
     * Returns the keys for this collection.
     *
     * @see https://www.php.net/manual/en/function.array-keys.php
     *
     * @return IterableCollection<int, K>
     */
    public function keys(): self
    {
        return new self((function (): Generator {
            foreach ($this->iterator as $key => $unused) {
                yield $key;
            }
        })());
    }

    /**
     * Applies a callback to each element of the collection.
     *
     * @see https://www.php.net/manual/en/function.array-map.php
     *
     * @template J
     * @param callable(V, K): J $mapCallback
     * @return IterableCollection<int, J>
     */
    public function map(callable $mapCallback): self
    {
        return new self((function () use ($mapCallback): Generator {
            foreach ($this->iterator as $key => $item) {
                yield $mapCallback($item, $key);
            }
        })());
    }

    /**
     * Iteratively reduce the array to a single value using a callback function.
     *
     * @see https://php.net/manual/en/function.array-reduce.php
     *
     * @template I
     * @param callable(I|null,V):I $callback
     * @param I|null $initial
     * @return I|null
     */
    public function reduce(callable $callback, $initial = null)
    {
        $carry = $initial;
        foreach ($this as $item) {
            $carry = $callback($carry, $item);
        }
        return $carry;
    }

    /**
     * Casts the current iterable object to an array.
     *
     * @see https://www.php.net/manual/en/function.iterator-to-array.php
     *
     * @param bool $use_keys
     * @return array<K, V>
     */
    public function toArray(bool $use_keys = true): array
    {
        return iterator_to_array($this, $use_keys);
    }

    /**
     * Returns all the values of the collection.
     *
     * @see https://www.php.net/manual/en/function.array-values.php
     *
     * @return IterableCollection<int, V>
     */
    public function values(): self
    {
        return new self((function (): Generator {
            foreach ($this->iterator as $item) {
                yield $item;
            }
        })());
    }

    /**
     * Applies a user supplied function to each leaf node of this collection.
     *
     * @see https://www.php.net/manual/en/function.array-walk-recursive.php
     *
     * @template J
     * @param callable(V, K, mixed|null): J $callback
     * @param mixed|null $userdata
     * @return IterableCollection<int, J>
     */
    public function walkRecursive(callable $callback, $userdata = null): self
    {
        return new self((function () use ($callback, $userdata): Generator {
            foreach ($this->iterator as $key => $item) {
                if (is_iterable($item)) {
                    foreach ((new self($item))->walkRecursive($callback, $userdata) as $nested) {
                        yield $nested;
                    }
                } else {
                    yield $callback($item, $key, $userdata);
                }
            }
        })());
    }

    /**
     * Applies a user supplied function to each element in the collection.
     *
     * @see https://www.php.net/manual/en/function.array-walk.php
     *
     * @template J
     * @param callable(V, K, J|null): void $callback
     * @param J|null $userdata
     * @return IterableCollection<K|mixed, V|mixed>
     */
    public function walk(callable $callback, $userdata = null): self
    {
        return new self((function () use ($callback, $userdata): Generator {
            foreach ($this->iterator as $key => $item) {
                $callback($item, $key, $userdata);
                yield $key => $item;
            }
        })());
    }

    /**
     * @template I
     * @template J
     * @param iterable<I, J> $iterator
     * @return IterableCollection<I, J>
     */
    public static function wrap(iterable $iterator): self
    {
        return new self($iterator);
    }
}
