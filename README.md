Iterable Collection
===================

[![pipeline status](https://gitlab.com/judahnator/iterable-collection/badges/master/pipeline.svg)](https://gitlab.com/judahnator/iterable-collection/commits/master)
[![coverage report](https://gitlab.com/judahnator/iterable-collection/badges/master/coverage.svg)](https://gitlab.com/judahnator/iterable-collection/-/commits/master)

The Gist
--------

This library allows you to use a fluent interface to manipulate iterable items.

The goal was to have at minimum feature parity with all the array_* functions, but the library falls a bit short of that goal due to a limitation of being unable to rewind generators. For that reason, some functions have been deliberately excluded.

Of course, if you can think of a good way to implement said functions feel free to submit a pull request.

A few additional functions were added as well that do not exist as native `array_*` methods.

Usage
-----

**Object Creation:**
```php
$o = new IterableCollection(['foo' => 'bar']);
// or
$o = IterableCollection::wrap(['foo' => 'bar']);
```

**Iteration:**
```php
foreach (IterableCollection::wrap(['foo' => 'bar']) as $key => $value) {
    // use $key and $value
}
```

**Check All True:**
```php
IterableCollection::wrap([true, false, true])->all();
# false

IterableCollection::wrap([true, true, true])->all();
# true

IterableCollection::wrap([1, 2, 'a'])->all('is_numeric');
# false
```

**Check Any True:**
```php
IterableCollection::wrap([true, false, true])->any();
# true

IterableCollection::wrap(['a', 'b', 'c'])->any(fn ($i) => $i === 'd');
# false
```

**Change Key Case:**
```php
$o = IterableCollection::wrap(['Hello' => 'World!']);

$o->changeKeyCase(CASE_LOWER);
# IterableCollection {
#   hello: "World!",
# }

$o->changeKeyCase(CASE_UPPER);
# IterableCollection {
#   HELLO: "World!",
# }
```

**Chunk:**
```php
$o = IterableCollection::wrap(range(1,4));

// No preserve keys (default)
$o->chunk(2);
# IterableCollection {
#   0: [
#     1,
#     2,
#   ],
#   1: [
#     3,
#     4,
#   ],
# }

// Preserve keys
$o->chunk(2, true);
# IterableCollection {
#   0: [
#     0 => 1,
#     1 => 2,
#   ],
#   1: [
#     2 => 3,
#     3 => 4,
#   ],
# }
```

**Column:**
```php
$o = IterableCollection::wrap([
    ['id' => 123, 'name' => 'Judah Wright',],
    ['id' => 456, 'name' => 'Jane Doe',],
]);

// Without column key (default)
$o->column('name');
# IterableCollection {
#   0: "Judah Wright",
#   1: "Jane Doe",
# }

// With a column key
$o->column('name', 'id');
# IterableCollection {
#   123: "Judah Wright",
#   456: "Jane Doe",
# }
```

**Count:**
```php
IterableCollection::wrap([1, 2, 3])->count();
# 3
```

**Combine:**
```php
$keys = ['foo', 'bing'];
$values = ['bar', 'baz'];
IterableCollection::wrap($keys)->combine($values);
# IterableCollection {
#   foo: "bar",
#   bing: "baz",
# }
```

**Count Values:**
```php
IterableCollection::wrap(['foo', 'bar', 'foo', 'baz'])->countValues();
# IterableCollection {
#   foo: 2,
#   bar: 1,
#   baz: 1,
# }
```

**Fill Keys:**
```php
IterableCollection::wrap(['a', 'b', 'c'])->fillKeys('foo');
# IterableCollection {
#   a: "foo",
#   b: "foo",
#   c: "foo",
# }
```

**Filter:**
```php
$inputs = [true, false, null, "Hello!"];

// Without a callback (default) returns keeps the "truthy" values
IterableCollection::wrap($inputs)->filter();
# IterableCollection {
#   0: true,
#   3: "Hello!",
# }

// With a callback you can do your own evaluation
IterableCollection::wrap($inputs)->filter(
    static fn ($value): bool => !is_null($value)
);
# IterableCollection {
#   0: true,
#   1: false,
#   3: "Hello!",
# }
```

**Flip:**
```php
IterableCollection::wrap(['key' => 'value'])->flip();
# IterableCollection {
#   value: "key",
# }
```

**In:**
```php
$o = IterableCollection::wrap(['foo', 'bar']);

$o->in('foo');
# true

$o->in('baz');
# false
```

**Key Exists:**
```php
$o = IterableCollection::wrap(['foo' => 'bar', 'bing' => 'baz']);

$o->keyExists('foo');
# true

$o->keyExists('asdf');
# false
```

**Keys:**
```php
IterableCollection::wrap(['foo' => 'bar', 'bing' => 'baz'])->keys();
# IterableCollection {
#   0: "foo",
#   1: "bing",
# }
```

**Map:**
```php
$o = IterableCollection::wrap(['John' => 'Smith', 'Jane' => 'Doe']);
$names = $o->map(
    fn ($firstname, $lastname): string => "{$firstname} {$lastname}"
);
# IterableCollection {
#   0: "Smith John",
#   1: "Doe Jane",
# }
```

**Reduce:**
```php
$o = IterableCollection::wrap([1, 2, 3]);
$o->reduce(static fn (int $carry, int $item): int => $carry + $item, 0);
# 6
```

**To Array:**
```php
$iterator = new ArrayIterator(['foo' => 'bar']);
print_r(IterableCollection::wrap($iterator)->toArray());
# Array
# (
#     [foo] => bar
# )
```

**Values:**
```php
IterableCollection::wrap(['key1' => 'value1', 'key2' => 'value2'])->values();
# IterableCollection {#2519
#   0: "value1",
#   1: "value2",
# }
```

**Walk Recursive:**
```php
$tree = [
    'limb1' => [
        'branch1' => 'leaf1', 
        'branch2' => 'leaf2',
    ], 
    'limb2' => 'leaf3'
];
IterableCollection::wrap($tree)->walkRecursive(
    fn ($item, $key, $userdata) => "{$userdata} - {$key} - {$item}",
    'tree',
);
# IterableCollection {
#   0: "tree - branch1 - leaf1",
#   1: "tree - branch2 - leaf2",
#   2: "tree - limb2 - leaf3",
# }
```

**Walk:**
```php
$o = IterableCollection::wrap(['Bears', 'Beets', 'Battlestar Galactica']);
$o->walk(
    function (&$item, &$key, int &$userdata): void {
        $item = strtoupper($item);
        $key = ['a', 'b', 'c'][$userdata];    
        $userdata++;
    },
    0,
);
# IterableCollection {
#   a: "BEARS",
#   b: "BEETS",
#   c: "BATTLESTAR GALACTICA",
# }
```

Deliberately Excluded Functions
-------------------------------

Items that would require the input to implement the [Iterator](https://php.net/manual/en/class.iterator.php) class, such as:
 * [array_key_first()](https://www.php.net/manual/en/function.array-key-first.php)
 * [array_key_last()](https://www.php.net/manual/en/function.array-key-last.php)
 * [current()](https://www.php.net/manual/en/function.current.php)
 * [end()](https://www.php.net/manual/en/function.end.php)
 * [key()](https://www.php.net/manual/en/function.key.php)
 * [next()](https://www.php.net/manual/en/function.next.php)
 
 Items that would require some form of indexing, ordering, or searching. These include:
 * [array_diff_assoc()](https://www.php.net/manual/en/function.array-diff-assoc.php)
 * [array_diff_key()](https://www.php.net/manual/en/function.array-diff-key.php)
 * [array_diff_uassoc()](https://www.php.net/manual/en/function.array-diff-uassoc.php)
 * [array_diff_ukey()](https://www.php.net/manual/en/function.array-diff-ukey.php)
 * [array_diff()](https://www.php.net/manual/en/function.array-diff.php)
 * [array_intersect_assoc()](https://www.php.net/manual/en/function.array-intersect-assoc.php)
 * [array_intersect_key()](https://www.php.net/manual/en/function.array-intersect-key.php)
 * [array_intersect_uassoc()](https://www.php.net/manual/en/function.array-intersect-uassoc.php)
 * [array_intersect_ukey()](https://www.php.net/manual/en/function.array-intersect-ukey.php)
 * [array_intersect()](https://www.php.net/manual/en/function.array-intersect.php)
 * [array_merge()](https://www.php.net/manual/en/function.array-merge.php)
 * [array_merge_recursive()](https://www.php.net/manual/en/function.array-merge-recursive.php)
 * [arsort()](https://www.php.net/manual/en/function.arsort.php)
 * [asort()](https://www.php.net/manual/en/function.asort.php)
 * [krsort()](https://www.php.net/manual/en/function.krsort.php)
 * [ksort()](https://www.php.net/manual/en/function.ksort.php)
 * [natcasesort()](https://www.php.net/manual/en/function.natcasesort.php)
 * [natsort()](https://www.php.net/manual/en/function.natsort.php)
 
 Some functions responsible for creating a new list, such as:
  * [array_fill()](https://www.php.net/manual/en/function.array-fill.php)